package com.zuitt.batch212;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {

        // Scanner class - to get user input and it is imported from the java.util package
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What's your name?");
        String myName = appScanner.nextLine().trim();
        // .nextLine() - accepts the input as a string
        // .trim() - removes the before and after whitespaces

        System.out.println("User name is: " + myName);

        System.out.println("What's your age?");
        int myAge = appScanner.nextInt();
        System.out.println("Age of the user: " + myAge);

        System.out.println("What's your grade?");
        double myGrade = appScanner.nextDouble();
        System.out.println("Your grade is: " + myGrade);
    }

}
