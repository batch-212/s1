package com.zuitt.batch212;

import java.sql.SQLOutput;
import java.util.Locale;

public class Identifier {

    public static void main(String[] args) {

        // Identifiers act as containers for values that are to be used by the program and manipulated further as needed

        // Identifier declaration - Syntax: dataType identifierName;
        int myNum;

        // Initialization
        // Literals - any  value which can be assigned to the identifier
        int x = 100;

        // Reassign the literals of our identifiers
        myNum = 29;
        System.out.println(myNum);

        myNum = 1;
        System.out.println(myNum);

        int age;
        char middle_name;


        // Constant Literals - Syntax - final dataType identifierName;
        final int PRINCIPAL = 1000;
        System.out.println(PRINCIPAL);



        // [Primitive Data Types]
        char letter = 'A';
        boolean isMarried = false;
        byte students = 127;
        short seats = 32_767;
        int localPopulation = 2_146_273_827; // underscore - will not affect the code, it acts as separator for readability

        System.out.println(localPopulation);

        long worldPopulation = 2_146_273_827L; // L - to specify that this is a long literal (optional)

        // decimal points
        float price = 12.99F; // F - to specify that this is a float literal (optional)
        double temperature = 12_745.43254;

        // getClass()- Get the datatype of an identifier
        System.out.println(((Object)temperature).getClass());


        // [NON PRIMITIVE DATA TYPE] - String, Arrays, Classes, Interface
        String name = "John Doe";
        System.out.println(name);

        String editedName = name.toLowerCase(); // string method
        System.out.println(editedName);

        System.out.println(name.getClass());

        // Escape characters
        System.out.println("c:\\windows\\desktop");

        System.out.println("Hi! My name is Charles. \n I'm from Quezon City!");

        // TypeCasting - the process of converting of data types
        // Implicit Casting/Widening Type Casting - automatic conversion

        // byte -> short -> int -> long -> float -> double
        // from smaller size to larger size
        String idNumberFormat = "000";
        byte idNumber = 1;

        String finalIdNumber = idNumberFormat + idNumber;
        System.out.println(finalIdNumber);

        byte num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println(total);
        System.out.println(((Object)total).getClass());


        // Explicit Casting/Narrowing Type Casting - manually convert one data type into another using the parenthesis
        // from larger size to smaller size
        // converting double into an int
        int num3 = 6;
        double num4 = 7.4;

        int anotherTotal = (int) (num3 + num4);
        System.out.println(anotherTotal);
        System.out.println(((Object)anotherTotal).getClass());

        String anotherNumber = "1" + 1;
        System.out.println(anotherNumber);
        System.out.println(anotherNumber.getClass());

        // Concatenation
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);

        // Converting strings to integer
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);

        // Converting integers to string
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade);
        System.out.println(stringGrade.getClass());



    }

}
