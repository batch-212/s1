package com.activity.s1;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.lang.Math;

public class AverageComputer {

    public static void main(String[] args) {

        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        Scanner input = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = input.nextLine().trim();

        System.out.println("Last Name:");
        lastName = input.nextLine().trim();

        System.out.println("First Subject Grade:");
        firstSubject = input.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = input.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = input.nextDouble();

        String fullName = firstName + " " + lastName;
        double average = (firstSubject + secondSubject + thirdSubject) / 3;
        String formattedAverage = String.format("%.2f", average);

        System.out.println("\nGood day, " + fullName + ".");
//        System.out.println("Your grade average is: " + Math.round(average));
        System.out.println("Your grade average is: " + formattedAverage);

    }

}
